import Endpoint from "../classes/endpoint"

const user = (service, filters) => {
  return new Endpoint(
    // NAME
    "user",

    // ENDPOINT URL
    { url: "/user" },

    // ROUTES
    {
      // Get current user
      current: {
        path: "/current",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const user = await service.current(req.user.id)

            if (user) {
              res.status(200).json(user)
            } else {
              res.status(200).json(false)
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Get user devices
      devices: {
        path: "/devices",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const devices = await service.getDevices(req)

            if (devices) {
              res.status(200).json(devices)
            } else {
              throw new Error("No devices were found.")
            }
          } catch (e) {
            next(e)
          }
        },
      },

      // Create new user
      create: {
        path: "/create",
        method: "POST",
        handler: async (req, res, next) => {
          try {
            if (req.body.admin) {
              throw new Error("No permission to perform this action")
            }
            const response = await service.create(req.body)
            res.status(200).json(response)
          } catch (e) {
            console.log(e)
            next(e)
          }
        },
      },

      // Update user
      update: {
        path: "/update/:id",
        method: "PUT",
        pre: filters.checkMongoId,
        handler: async (req, res, next) => {
          try {
            await service.update(req, req.body)
            res.status(200).json({ message: "User updated successfully" })
          } catch (e) {
            console.log(e)
            next(e)
          }
        },
      },

      // Delete user
      delete: {
        path: "/delete/:id",
        method: "DELETE",
        pre: filters.checkMongoId,
        handler: async (req, res, next) => {
          try {
            if (req.user.id !== req.params.id) {
              throw new Error("No permission to perform this action")
            }

            await service.delete(req.params.id)
            res.status(200).json({ message: "User deleted successfully" })
          } catch (e) {
            console.log(e)
            next(e)
          }
        },
      },

      // Get all user devices (temporary)
      all: {
        path: "/all",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const users = await service.all()
            res.status(200).json(users)
          } catch (e) {
            console.log(e)
            next(e)
          }
        },
      },

      // Get by alias (!important must be in the end of this chain)
      alias: {
        path: "/:alias",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const user = await service.getByAlias(req)

            if (user) {
              res.status(200).json(user)
            } else {
              res.status(404).json({
                message: "No user found with that username",
              })
            }
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default user
