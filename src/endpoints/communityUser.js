import Endpoint from "../classes/endpoint"

const communityUser = (service, filters) => {
  const { checkMongoId, isOwner } = filters

  return new Endpoint(
    // Name
    "community-user",

    // MAIN URL
    { url: "/community/user" },

    // HANDLER ROUTES
    {
      // request to join community
      request: {
        path: "/request/:id",
        method: "GET",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            const resCommunityUser = await service.request(
              req.user.id,
              req.params.id
            )

            res.status(200).json({ id: resCommunityUser.id })
          } catch (e) {
            next(e)
          }
        },
      },

      // approve user for the community
      approve: {
        path: "/approve/:id",
        method: "PUT",
        pre: [checkMongoId, isOwner],
        handler: async (req, res, next) => {
          try {
            await service.approve(req.params.id)

            res.status(200).json({ message: "Community accepted successfully" })
          } catch (e) {
            next(e)
          }
        },
      },

      // reject or remove the request to the community owner
      reject: {
        path: "/reject/:id",
        method: "DELETE",
        pre: checkMongoId,
        handler: async (req, res, next) => {
          try {
            await service.reject(req.params.id)

            res.status(200).json({
              message: "Removed/Rejected user from community was successfully",
            })
          } catch (e) {
            next(e)
          }
        },
      },

      // get all users from a community
      all: {
        path: "/all/:id",
        method: "GET",
        pre: [checkMongoId, isOwner],
        handler: async (req, res, next) => {
          try {
            const communityUsers = await service.all(req.params.id)

            res.status(200).json(communityUsers)
          } catch (e) {
            next(e)
          }
        },
      },

      // get a number with the number of members of a community
      count: {
        path: "/count/:alias",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const communityUsers = await service.count(req.params.alias)

            res.status(200).json(communityUsers)
          } catch (e) {
            next(e)
          }
        },
      },

      // get all requests i made to communities
      pending: {
        path: "/pending",
        method: "GET",
        handler: async (req, res, next) => {
          try {
            const pendingUsers = await service.pending(req.user.id)

            res.status(200).json(pendingUsers)
          } catch (e) {
            next(e)
          }
        },
      },
    }
  )
}

export default communityUser
