import nodemailer from "nodemailer"
import { EMAIL_SERVICE, EMAIL_USER, EMAIL_PASS, APP_NAME } from "./config"
import { API_BASE_URL, FRONTEND_URL } from "./config"
const options = {
  service: EMAIL_SERVICE,
  auth: {
    user: EMAIL_USER,
    pass: EMAIL_PASS,
  },
}

export const transporter = ({ email, subject = "", html = "" }) => {
  try {
    if (!email) {
      throw new Error("Need email recipient to send mail")
    }

    const transporter = nodemailer.createTransport(options)

    transporter.sendMail({ to: email, subject, html })
  } catch (e) {
    console.log(e)
    throw new Error(e)
  }
}

export const confirmEmail = (user, emailToken) => {
  const link = `${FRONTEND_URL}/confirm-email/${emailToken}`

  const subject = `${APP_NAME} - Email Confirmation`

  // TODO: import better structure for the email template
  const html = `
    <div style="text-align: center;">
      <h1>${APP_NAME}</h1>
      <h4>Hi ${user.displayName}, welcome!</h4>
      </br>
      <p>
        You've successfully created an account on the ${APP_NAME},</br>
        only need to click on the link below to confirm your account and start using the platform.
      </p>
      <a target="_blank" href="${link}">CONFIRM ACCOUNT</a>
      <b style="text-align: center;">${getYear()}</b>
      </br>
      <small style="text-align: center;">If you are receiving this email by mistake, just ignore.</small>
    </div>
    `

  transporter({ email: user.email, subject, html })
}

export const resetPasswordEmail = (email, resetPasswordToken) => {
  const link = `${API_BASE_URL}/api/auth/change-password/${resetPasswordToken}`

  const subject = `${APP_NAME} - Reset Password`

  // TODO: import better structure for the email template
  const html = `
    <div style="text-align: center;">
      <h1>${APP_NAME}</h1>
      <h4>Reset password email</h4>
      </br>
      <p>
        You've requested to reset your password. Reset your password by clicking</br>
        on the link below where you will be redirected to the reset password page.</br>
      </p>
      </br>
      <a target="_blank" href="${link}">RESET PASSWORD</a>
      </br>
      </br>
      <p>
        If your account was locked, it will be unlocked after you change the password.</br>
        This link will expire in 24h from now.
      </p>
      </br>
      </br>
      <b style="text-align: center;">${getYear()}</b>
      </br>
      <small style="text-align: center;">If you are receiving this email by mistake, just ignore.</small>
    </div>
    `

  transporter({ email, subject, html })
}

function getYear() {
  const today = new Date()
  return today.getFullYear()
}
