const schema = ({ Schema }) => {
  return {
    requester: { type: Schema.Types.ObjectId, ref: "User" },
    recipient: { type: Schema.Types.ObjectId, ref: "User" },

    state: {
      type: Number,
      enum: [
        1, // unread
        2, // read
        3, // answered
      ],
      default: 1,
    },

    date: {
      type: Date,
      default: Date.now,
    },

    text: {
      type: String,
      maxlength: [255, "Text is only alowed to 255 characters maximum"],
    },
  }
}

export default schema
