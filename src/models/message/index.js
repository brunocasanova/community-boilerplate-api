import schema from "./schema"

const Message = {
  name: "Message",
  schema: schema,
  options: {
    timestamps: true,
  },
}

export default Message
