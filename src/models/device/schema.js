const schema = ({ Schema }) => {
  return {
    User: { type: Schema.Types.ObjectId, ref: "User" },

    type: String,
    browser: String,
    os: String,
    ua: String,

    token: {
      type: String,
      required: true,
    },
  }
}

export default schema
