import schema from "./schema"

const Device = {
  name: "Device",
  schema: schema,
  options: {
    timestamps: true,
  },
}

export default Device
