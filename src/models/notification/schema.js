const schema = ({ Schema }) => {
  return {
    User: { type: Schema.Types.ObjectId, ref: "User" },

    UserConnection: { type: Schema.Types.ObjectId, ref: "UserConnection" },
    Message: { type: Schema.Types.ObjectId, ref: "Message" },
    Community: { type: Schema.Types.ObjectId, ref: "Community" },

    subject: {
      type: String,
      enum: ["request", "accept", "reject"],
    },

    state: {
      type: Number,
      enum: [
        0, // none
        1, // not seen
        2, // seen
      ],
      default: 0,
    },

    date: {
      type: Date,
      default: Date.now,
    },

    text: {
      type: String,
      maxlength: [255, "Text is only alowed to 255 characters maximum"],
    },
  }
}

export default schema
