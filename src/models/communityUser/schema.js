const schema = ({ Schema }) => {
  return {
    Community: { type: Schema.Types.ObjectId, ref: "Community" },
    User: { type: Schema.Types.ObjectId, ref: "User" },

    status: {
      type: Number,
      enums: [
        1, //'requested',
        2, //'accepted',
      ],
      default: 1,
    },
  }
}

export default schema
