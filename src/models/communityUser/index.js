import schema from "./schema"

const CommunityUser = {
  name: "CommunityUser",
  schema: schema,
  options: {
    timestamps: true,
  },
}

export default CommunityUser
