const schema = ({ Schema }) => {
  return {
    Community: { type: Schema.Types.ObjectId, ref: "Community" },

    date: {
      type: Date,
      default: Date.now,
    },

    title: {
      type: String,
      maxlength: [120, "Text is only alowed to 120 characters maximum"],
    },
    text: {
      type: String,
      maxlength: [255, "Text is only alowed to 255 characters maximum"],
    },
  }
}

export default schema
