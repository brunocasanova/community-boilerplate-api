const schema = ({ Schema }) => {
  return {
    requester: { type: Schema.Types.ObjectId, ref: "User" },
    recipient: { type: Schema.Types.ObjectId, ref: "User" },
    status: {
      type: Number,
      enums: [
        0, // none/no response
        1, //'requested',
        2, //'connected'
        3, // 'blocked'
      ],
      default: 1,
    },
  }
}

export default schema
