import schema from "./schema"
import methods from "./methods"

const UserConnection = {
  name: "UserConnection",
  schema: schema,
  methods: methods,
  options: {
    timestamps: true,
  },
}

export default UserConnection
