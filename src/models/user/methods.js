const methods = ({ models }) => {
  const findOneAndDelete = {
    // HOOK
    type: "post",
    option: "findOneAndDelete",
    function: async user => {
      const {
        Community,
        CommunityUser,
        UserConnection,
        Message,
        Device,
      } = models

      try {
        // delete the community that he is owner or the connections where he belongs
        if (user.Community) {
          await Community.findOneAndDelete({ owner: user.id })
          await CommunityUser.findOneAndDelete({ User: user.id })
        }

        // delete the connections that he is involved
        await UserConnection.deleteMany({
          $or: [{ requester: user.id }, { recipient: user.id }],
        })

        // delete the conversation if both users dont exist anymore
        await Message.deleteMany({
          $or: [{ requester: user.id }, { recipient: user.id }],
        })

        // delete the sessions and devices
        await Device.deleteMany({
          User: user,
        })
      } catch (e) {
        throw new Error(e)
      }
    },
  }

  const indexes = {
    // INDEXING

    type: "index",
    content: {
      alias: "text",
      displayName: "text",
      "information.sex": "text",
      "information.nationality": "text",
      "information.location.continent": "text",
      "information.location.country": "text",
      "information.location.region": "text",
      "information.location.city": "text"
    },
    options: {
      weights: {
        alias: 2,
        displayName: 1,
        "information.sex": 3,
        "information.nationality": 3,
        "information.location.continent": 4,
        "information.location.country": 3,
        "information.location.region": 4,
        "information.location.city": 4
      },
    },
  }

  return [indexes, findOneAndDelete]
}

export default methods
