import validator from "validator"
import {
  nationalities,
  languages,
  professions,
} from "../../helpers"

const visibilityDefault = {
  type: Number,
  enum: [
    1, // private
    2, // friends
    3, // public
  ],
  default: 3,
}

const questionsDefault = {
  type: String,
  enum: [
    1, // "Yes"
    2, // "No"
    3, // "I don't know"
  ],
}

const smallText = [55, "Text is only alowed to 55 characters maximum."]
const mediumText = [120, "Text is only alowed to 120 characters maximum."]
const largeText = [255, "Text is only alowed to 255 characters maximum."]
const extraLargeText = [5000, "Text is only alowed to 5000 characters maximum."]

const schema = ({ Schema }) => {
  return {
    admin: {
      type: Boolean,
      default: false,
    },

    confirmed: {
      type: Boolean,
      default: false,
    },

    // Credentials
    alias: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    displayName: {
      type: String,
      maxlength: [20, "Username is alowed to 20 characters maximum"],
      minlength: [3, "Username must be at least 3 characters"],
      required: true,
      unique: true,
    },
    hash: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      trim: true,
      lowercase: true,
      unique: true,
      required: "Email address is required",
      validate: [validator.isEmail, "Please fill a valid email address"],
      match: [
        /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
        "Please fill a valid email address",
      ],
    },

    lastLogin: Date,

    // Password attempts
    passwordAttempts: {
      type: Number,
      default: 0,
    },
    passwordLock: Date,

    // INFORMATION //////////////////////////////////////////////////////////////

    // user general informations
    information: {
      // BASIC INFO

      sex: {
        type: String,
        enum: ["M", "F", "Other"],
      },

      age: Number,

      nationality: {
        type: String,
        enum: nationalities,
      },

      // LOCATION

      location: [
        {
          continent: {
            type: String,
            maxlength: smallText,
          },
          country: {
            type: String,
            maxlength: smallText,
          },
          region: {
            type: String,
            trim: true,
            maxlength: smallText,
          },
          city: {
            type: String,
            trim: true,
            maxlength: smallText,
          },
        },
      ],

    },

    // RIBBONS //////////////////////////////////////////////////////////////

    // Search Ribbons
    ribbons: {
      1: Boolean,
      2: Boolean,
      3: Boolean,
      4: Boolean,
      5: Boolean,
    },


    // Visibility options
    visibility: {
      // Hide/show all profile
      profile: visibilityDefault,

      // Hide/show all ribbons
      ribbons: visibilityDefault,

      // Hide/show all questions
      //questions: visibilityDefault,

      // simple options
      email: visibilityDefault,
      sex: visibilityDefault,
      age: visibilityDefault,
      ageRange: visibilityDefault,
      nationality: visibilityDefault,
      location: visibilityDefault,
    },

    // RELATIONSHIPS

    // Community only
    Community: { type: Schema.Types.ObjectId, ref: "Community" },
  }
}

export default schema
