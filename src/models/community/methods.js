const methods = ({ models }) => {
  const findOneAndDelete = {
    // HOOK
    type: "post",
    option: "findOneAndDelete",
    function: async community => {
      const { User, CommunityUser } = models

      try {
        // Remove the Community id in the owner
        await User.updateMany(
          { Community: community.id },
          { $unset: { Community: true } }
        )

        // Remove all links in CommunityUsers
        await CommunityUser.deleteMany({
          Community: community.id,
        })
      } catch (e) {
        console.log(e)
        throw new Error(e)
      }
    },
  }

  const indexes = {
    // INDEXING
    type: "index",
    content: {
      alias: "text",
      displayName: "text",
      tag: "text",
      description: "text",
    },
    options: {
      weights: {
        alias: 2,
        displayName: 1,
        tag: 1,
        description: 5,
      },
    },
  }

  return [indexes, findOneAndDelete]
}

export default methods
