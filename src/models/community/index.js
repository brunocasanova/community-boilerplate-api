import schema from "./schema"
import methods from "./methods"

const Community = {
  name: "Community",
  schema: schema,
  methods: methods,
  options: { timestamps: true },
}

export default Community
