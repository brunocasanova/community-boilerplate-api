const schema = ({ Schema }) => {
  return {
    owner: {
      type: Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },

    listed: {
      type: Boolean,
      default: true,
    },

    alias: {
      type: String,
      required: true,
      unique: true,
    },

    displayName: {
      type: String,
      maxlength: [25, "Display name is only alowed to 25 characters maximum"],
      required: true,
      unique: true,
    },

    tag: {
      type: String,
      maxlength: [5, "Tag is only alowed to 5 characters maximum"],
      required: true,
      unique: true,
    },

    description: {
      type: String,
      maxlength: [120, "Text is only alowed to 120 characters maximum"],
    },
  }
}

export default schema
