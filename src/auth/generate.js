import jwt from "jsonwebtoken"
import { ACCESS_SECRET, REFRESH_SECRET } from "../config"

// Confirm and password reset duration emails
export const confirmEmailDuration = "1d"
export const restPasswordDuration = "1d"

// Auth duration tokens
const accessDuration = "10m"
const refreshDuration = "24h"
const refreshDurationRemember = "365d"

export const generateAccessToken = id => {
  return jwt.sign({ id }, ACCESS_SECRET, { expiresIn: accessDuration })
}

export const generateRefreshToken = (id, remember) => {
  const duration = {
    expiresIn: remember ? refreshDurationRemember : refreshDuration,
  }

  return jwt.sign({ id }, REFRESH_SECRET, duration)
}
