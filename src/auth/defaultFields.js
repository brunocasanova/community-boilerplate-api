// user miniatures info
export const portraitFields = ["_id", "alias", "displayName"].join(" ")

// user search miniatures info
export const userSearchPortraitFields = [
  "_id",
  "alias",
  "displayName",
  "Community",
].join(" ")

// user community miniatures info
export const userCommunityPortraitFields = ["_id", "alias", "displayName"].join(
  " "
)

// community miniatures info
export const communityPortraitFields = ["-owner"].join(" ")
