// Routes that can be accessed without jwt token

export default {
  path: [
    { url: /\/socket.io\/*/, methods: ["GET"] },
    { url: /\/api\/health-check/, methods: ["GET"] },
    { url: /\/api\/auth\/login/, methods: ["POST"] },
    { url: /\/api\/auth\/refresh/, methods: ["POST"] },
    { url: /\/api\/auth\/confirm-email\/*/, methods: ["GET"] },
    { url: /\/api\/auth\/reset-password/, methods: ["POST"] },
    { url: /\/api\/auth\/change-password\/*/, methods: ["GET"] },
    { url: /\/api\/user\/create/, methods: ["POST"] },
  ],
}
