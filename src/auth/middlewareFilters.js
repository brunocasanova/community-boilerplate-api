import { customValidators } from "../helpers"

// MIDDLEWARES FILTERS

export default models => {
  const { User, Community, UserConnection } = models
  return {
    // If the user is admin
    isAdmin: async (req, res, next) => {
      try {
        const user = await User.findById(req.user.id)

        if (!user.admin) {
          throw new Error("No permission to perform this action")
        }

        next()
      } catch (e) {
        next(e)
      }
    },

    // If user is blocked by the other
    isBlocked: async (req, res, next) => {
      try {
        const connectionBlocked = await UserConnection.findById(req.params.id)

        if (!connectionBlocked) {
          const blocked = await UserConnection.find({
            requester: req.params.id,
            recipient: req.user.id,
            status: 3,
          }).exec()

          if (blocked.length >= 1) {
            throw new Error("Requester is blocked by recipient")
          }
        } else if (
          connectionBlocked.recipient === req.user.id &&
          connectionBlocked.status === 3
        ) {
          throw new Error("Requester is blocked by recipient")
        }

        next()
      } catch (e) {
        next(e)
      }
    },

    // Community owner related
    isOwner: async (req, res, next) => {
      try {
        const community = await Community.find({
          owner: req.user.id,
        }).exec()

        if (community.length < 1) {
          throw new Error("No permission to perform this action")
        }

        next()
      } catch (e) {
        next(e)
      }
    },

    // Assure that is a mongo id, never trust client :D
    checkMongoId: async (req, res, next) => {
      try {
        if (!customValidators.isMongoId(req.params.id)) {
          throw new Error("Need a valid id to perform this action")
        }

        next()
      } catch (e) {
        next(e)
      }
    },

    // Add more here...
  }
}
