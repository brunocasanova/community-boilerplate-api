import Service from "../classes/service"

const community = ({ models }) => {
  const { Community, CommunityUser, User } = models

  return new Service(
    // Name
    "community",

    // Methods
    {
      // get current community
      current: async userId => {
        const user = await User.findById(userId)

        if (user.Community) {
          return await Community.findById({
            _id: user.Community,
          })
        }

        return false
      },

      // Create a new community
      create: async (userId, communityParam) => {
        const user = await User.findById(userId)

        if (user.Community) {
          throw new Error("User already have a community")
        }

        const alias = communityParam.name
          .toLowerCase()
          .replace(/\s/g, "-")
          .trim()
        const checkName = await Community.findOne({ alias: alias })
        const tag = communityParam.tag.toUpperCase()

        if (checkName) {
          throw new Error(
            `Community name (${communityParam.name}) is already taken`
          )
        }

        const community = new Community(communityParam)

        // Relate the community with the user and user with community
        community.owner = user
        user.Community = community

        community.displayName = communityParam.name.trim()
        community.alias = alias
        community.tag = tag
        delete communityParam.name
        delete communityParam.tag

        // Add the owner as approved user on the Community
        const communityUser = new CommunityUser({
          Community: community,
          User: user,
          status: 2,
        })

        // Validate before save, if one fails, the other fails too
        await user.validate()
        await community.validate()
        await communityUser.validate()

        await user.save()
        await community.save()
        await communityUser.save()

        return community
      },

      // Update a community
      update: async (id, communityParam) => {
        const community = await Community.findById(id)

        if (!community) {
          throw new Error("Community not found")
        }

        if (communityParam.name) {
          const alias = communityParam.name
            .toLowerCase()
            .replace(/\s/g, "-")
            .trim()

          // check if the name doesnt exist, let pass the name
          if (
            community.alias !== alias &&
            (await Community.findOne({ alias: alias }))
          ) {
            throw new Error(
              `Community name (${communityParam.name}) is already taken`
            )
          }

          community.displayName = communityParam.name.trim()
          community.alias = alias
          delete communityParam.name
        }

        if (communityParam.tag) {
          const tag = communityParam.tag.toUpperCase()
          community.tag = alias
          delete communityParam.tag
        }

        // merge differences
        await Object.assign(community, communityParam)

        await community.save()
      },

      // delete community
      delete: async id => await Community.findByIdAndDelete(id),

      // (Temporary)
      all: async (limit = 25) => {
        return await Community.find({
          listed: true,
        }).limit(+limit)
      },

      getByAlias: async req => {
        const { alias } = req.params

        return await Community.findOne({ alias: alias }).populate({
          path: "owner",
          select: "alias displayName",
        })
      },

      // Add more here...
    }
  )
}

export default community
