import Service from "../classes/service"

const communityAnnouncement = ({ models }) => {
  const { Announcement, Community } = models

  return new Service(
    // Name
    "community-announcement",

    // Methods
    {
      // create a new announcement
      create: async (userId, announcementParams) => {
        const community = await Community.find({
          owner: userId,
        })

        if (community.length < 1) {
          throw new Error("No permission to perform this action")
        }

        const announcement = new Announcement(announcementParams)

        // Link it with the community
        announcement.Community = community[0]

        await announcement.save()

        return announcement
      },

      // update an existing announcement
      update: async (id, announcementParams) => {
        const announcement = await Announcement.findById(id)

        // merge differences
        await Object.assign(announcement, announcementParams)

        await announcement.save()
      },

      // delete an existing announcement
      delete: async id => await Announcement.findByIdAndDelete(id),

      // Add more here...
    }
  )
}

export default communityAnnouncement
