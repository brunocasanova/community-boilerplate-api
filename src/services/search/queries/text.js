export const userTextQuery = (search, searchFilter) => {
  const searchText = userSex(search)

  return {
    $or: [
      { alias: searchFilter },
      { displayName: searchFilter },
      { $text: { $search: `${searchText}` } },
    ],
  }
}

function userSex(search) {
  const searchLower = search.toLowerCase()

  if (searchLower === "male" || searchLower === "man") {
    return "M"
  }

  if (searchLower === "female" || searchLower === "woman") {
    return "F"
  }

  return search
}

export const communityTextQuery = (search, searchFilter) => {
  const searchText = userSex(search)

  return {
    $or: [
      { alias: searchFilter },
      { displayName: searchFilter },
      { tag: searchFilter },
      { $text: { $search: `${searchText}` } },
    ],
  }
}
