export const queryInformation = (query, queryParams) => {
  const information = queryParams.information

  // lets query the age ranges and locations, separatedly from the other query
  queryAge(query, queryParams)

  for (let keyParam in information) {
    // Lets see what parameters where asked for
    if (keyParam) {
      // if its one of these fields dont join to the query here, lets do it separatedly
      if (keyParam === "ageRange" || keyParam === "age") {
        continue
      }

      // if its location lets do it separated
      if (keyParam === "location" || keyParam === "location") {
        queryLocation(query, information, keyParam)
        continue
      }

      //if parameter value is a string or boolean
      if (
        typeof information[keyParam] === "string" ||
        typeof information[keyParam] === "boolean"
      ) {
        query[`information.${keyParam}`] = information[keyParam]
        continue
      }

      //if parameter value is a array
      if (Array.isArray(information[keyParam])) {
        // start the AND array if it didnt had one
        if (!Array.isArray(query["$and"])) {
          query["$and"] = []
        }

        query["$and"].push({
          $or: information[keyParam].map(n => ({
            [`information.${keyParam}`]: n,
          })),
        })
      }
    }
  }
}

export const queryRibbons = (query, queryParams) => {
  const information = queryParams.ribbons

  for (let param in information) {
    if (param) {
      query[`ribbons.${param}`] = information[param]
    }
  }
}

function queryLocation(query, information, keyParam) {
  // start the AND array if it didnt had one
  if (!Array.isArray(query["$and"])) {
    query["$and"] = []
  }

  // Lets push all the options to the AND array
  query["$and"].push({
    $or: information[keyParam].map(params => {
      let result = {}

      for (let key in params) {
        result[`information.${keyParam}.${key}`] = params[key]
      }

      return result
    }),
  })
}

function queryAge(query, queryParams) {
  const { age, ageRange } = queryParams.information

  // If age is in normal range
  if (age) {
    query["information.age"] = { $gte: age.min, $lte: age.max }

    // If age is in age group range
  } else if (ageRange && ageRange.length > 0) {
    const ageSplit = (ageRange.match(/\d+/g) || []).map(n => parseInt(n))

    let queryOr = []

    if (ageSplit.length === 1) {
      queryOr.push({ "information.age": { $gte: ageSplit[0], $lte: 120 } })
    } else {
      queryOr.push({
        "information.age": { $gte: ageSplit[0], $lte: ageSplit[1] },
      })
    }

    queryOr.push({ "information.ageRange": ageRange })

    // start the AND array if it didnt had one
    if (!Array.isArray(query["$and"])) {
      query["$and"] = []
    }

    query["$and"].push({
      $or: queryOr,
    })
  }
}
