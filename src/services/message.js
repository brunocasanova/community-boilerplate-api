import Service from "../classes/service"

const message = ({ models }) => {
  const { Message } = models

  return new Service(
    // Name
    "message",

    // Methods
    {
      // send a message to a user
      send: async (requester, recipient, messageParams) => {
        const message = new Message(messageParams)

        message.requester = requester
        message.recipient = recipient
        message.status = 1
        message.state = 1

        await message.save()

        return message
      },

      // delete a message
      delete: async (id, userId) => {
        const message = await Message.findById(id)

        if (!message) {
          throw new Error("No message was found")
        }

        // check if the message belongs to the the user
        if (message.requester.toString() !== userId) {
          throw new Error("No permission to make this action")
        }

        return await Message.findByIdAndDelete(id)
      },

      read: async id => {
        const message = await Message.findById(id)

        if (!message) {
          throw new Error("No message was found")
        }

        // Set the message to read state (2)
        await Message.findOneAndUpdate({ _id: id }, { $set: { state: 2 } })
      },

      conversation: async (requester, recipient, limit = 25) => {
        // limit to save the conversation on database
        const conversationLimit = 100

        limit = +limit

        if (typeof limit !== "number") {
          throw new Error("Option is not accepted")
        }

        const conversation = await Message.find({
          $or: [
            { requester: requester, recipient: recipient },
            { requester: recipient, recipient: requester },
          ],
        })
          .limit(+limit)
          .sort({ date: "asc" })

        if (conversation.length > conversationLimit) {
          const deleteId = conversation[0].id

          await Message.findByIdAndDelete(deleteId)
        }

        return conversation
      },

      // Add more here...
    }
  )
}

export default message
