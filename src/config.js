import dotenv from "dotenv"
dotenv.config()

const defaults = {
  port: 9000,
  host: "localhost",
  protocol: "http://",

  mongoPort: "27017",
  mongoName: "defaultname",

  frontendPort: 8000,
}

export const APP_NAME = "NODE_MONGO_API"

// ENVIRONMENT

const environment = process.env.NODE_ENV || "dev"

export const ENV = environment == "dev" ? "development" : "production"
export const DEV = environment == "dev"
export const PROD = environment == "prod"

// SERVER

export const PROTOCOL = process.env.PROTOCOL || defaults.protocol
export const HOST = process.env.HOST || defaults.host
export const PORT = process.env.PORT || defaults.port

export const API_BASE_URL =
  process.env.API_BASE_URL || [PROTOCOL, HOST, ":", PORT].join("")

// FRONTEND

const defaultFrontendUrl = `${defaults.protocol}${defaults.host}:${defaults.frontendPort}`

export const FRONTEND_URL = process.env.FRONTEND_URL || defaultFrontendUrl

// CORS
export const CORS_WHITELIST = [FRONTEND_URL]

// MONGO

export const MONGOENV = environment == "dev" ? defaults.host : "mongo"
export const MONGOPORT = process.env.MONGO_PORT || defaults.mongoPort
export const MONGONAME = process.env.MONGO_NAME || defaults.mongoName

// EMAIL

export const EMAIL_SERVICE = process.env.EMAIL_SERVICE
export const EMAIL_USER = process.env.EMAIL_USER
export const EMAIL_PASS = process.env.EMAIL_PASSWORD

// JWT
export const ACCESS_SECRET = process.env.ACCESS_SECRET
export const REFRESH_SECRET = process.env.REFRESH_SECRET
export const EMAIL_SECRET = process.env.EMAIL_SECRET
export const RESET_PASSWORD_SECRET = process.env.RESET_PASSWORD_SECRET
