import bodyParser from "body-parser"
import cors from "cors"
import helmet from "helmet"
import errorHandler from "./error-handler"
import { CORS_WHITELIST } from "./config"

// Cors Options
const corsOptions = (req, next) => {
  const isListed = CORS_WHITELIST.indexOf(req.header("Origin")) !== -1
  let options = {
    optionsSuccessStatus: 200,
    credentials: true,
  }

  options = { origin: isListed, ...options }
  next(null, options)
}

export const errorHandlerMiddleware = [
  ["not-found", (req, res) => res.sendStatus(404)],
  ["error-handler", errorHandler],
]

export const middlewares = [
  ["body-parser", bodyParser.urlencoded({ extended: false })],
  ["body-parser json", bodyParser.json()],
  ["helmet", helmet()],
  ["cors", cors(corsOptions)],
]
