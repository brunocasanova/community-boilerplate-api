function server() {
  this.server = this.app.listen(
    this.port,
    console.log(`[SERVER|CONNECTION]`),
    console.log(`[SERVER]: Connected at ${this.base_url}.`)
  )
}

export default server
