import request from "supertest"
import chai from "chai"
import jwt from "jsonwebtoken"
import app from "../../../src/app"
import { EMAIL_SECRET } from "../../../src/config"
import { Community, User, Announcement } from "../../../src/models"
import communityDummy from "../../dummies/communityDummy.json"
import userCommunityDummy from "../../dummies/dummyUsers/userCommunityDummy.json"
import announcementDummy from "../../dummies/announcementDummy.json"

const expect = chai.expect
const urlBase = "/api/v1/community"

describe("COMMUNITY ACTIONS", () => {
  let user = null
  let token = null
  let communityId = null
  let announcementId = null

  // CREATE DUMMY USER

  before(async () => {
    try {
      const userRes = await request(app)
        .post(`/api/v1/user/create`)
        .send(userCommunityDummy)

      const createdUser = await User.findById(userRes.body.id)
      user = createdUser

      const confirmToken = jwt.sign({ id: user.id }, EMAIL_SECRET, {
        expiresIn: "1d",
      })

      await request(app).get(`/api/v1/user/confirmation/${confirmToken}`)

      const AuthRes = await request(app)
        .post(`/api/v1/user/authenticate`)
        .send({
          username: userCommunityDummy.username,
          password: userCommunityDummy.password,
        })

      token = AuthRes.body.token
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // CREATE COMMUNITY

  it("POST / It should create a community and assign a owner", async () => {
    try {
      const res = await request(app)
        .post(`${urlBase}/create`)
        .send(communityDummy)
        .set("Authorization", `Bearer ${token}`)

      expect(res.status).to.equal(200)

      communityId = res.body.id
      const createdCommunity = await Community.findById(communityId)

      expect(createdCommunity).to.have.a.property("_id")
      expect(createdCommunity.owner._id.toString()).to.equal(user.id)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // CREATE COMMUNITY ANNOUNCEMENT

  it("POST / It should create a community announcement", async () => {
    try {
      const res = await request(app)
        .post(`${urlBase}/announcement/create`)
        .send(announcementDummy)
        .set("Authorization", `Bearer ${token}`)

      expect(res.status).to.equal(200)

      announcementId = res.body.id

      const createdAnnouncement = await Announcement.findById(announcementId)

      expect(createdAnnouncement).to.have.property("_id")
      expect(createdAnnouncement.Community._id.toString()).to.equal(communityId)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // UPDATE COMMUNITY ANNOUNCEMENT
  it("PUT / It should update a community announcement", async () => {
    const newTitle = "Updated Announcement Title"
    try {
      const res = await request(app)
        .put(`${urlBase}/announcement/update/${announcementId}`)
        .send({ title: newTitle })
        .set("Authorization", `Bearer ${token}`)

      expect(res.status).to.equal(200)

      const updatedAnnouncement = await Announcement.findById(announcementId)

      expect(updatedAnnouncement.title).to.equal(newTitle)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // DELETE COMMUNITY ANNOUNCEMENT

  it("DELETE / It should delete a community announcement", async () => {
    try {
      const res = await request(app)
        .delete(`${urlBase}/announcement/delete/${announcementId}`)
        .set("Authorization", `Bearer ${token}`)

      expect(res.status).to.equal(200)

      const announcement = await Announcement.findById(announcementId)

      expect(announcement).to.equal(null)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // UPDATE COMMUNITY

  it("PUT / It should update the community", async () => {
    try {
      const newCommunityName = "updatedCommunity"

      const res = await request(app)
        .put(`${urlBase}/update/${communityId}`)
        .send({ name: newCommunityName })
        .set("Authorization", `Bearer ${token}`)

      expect(res.status).to.equal(200)

      const updatedCommunity = await Community.findById(communityId)

      expect(updatedCommunity.name).to.equal(newCommunityName)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // LIST COMMUNITY BY ID

  it("GET / It should list the community by id", async () => {
    try {
      const resCommunityById = await request(app)
        .get(`${urlBase}/id/${communityId}`)
        .set("Authorization", `Bearer ${token}`)

      expect(resCommunityById.status).to.equal(200)

      const communityById = await Community.findById(communityId)

      expect(resCommunityById.body).to.have.a.property("_id")
      expect(resCommunityById.body._id.toString()).to.equal(
        communityById._id.toString()
      )
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // LIST CURRENT COMMUNITY

  it("GET / It should list current community that user is on", async () => {
    try {
      const resCurrentCommunity = await request(app)
        .get(`${urlBase}/current`)
        .set("Authorization", `Bearer ${token}`)

      expect(resCurrentCommunity.status).to.equal(200)
      expect(resCurrentCommunity.body).to.have.a.property("_id")
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // LIST ALL COMMUNITIES

  it("GET / It should list all the listed communities", async () => {
    try {
      const resCommunityList = await request(app)
        .get(`${urlBase}/all?limit=2`)
        .set("Authorization", `Bearer ${token}`)

      expect(resCommunityList.status).to.equal(200)

      expect(resCommunityList.body).to.be.an("array").to.have.lengthOf(1)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // DELETE COMMUNITY

  it("DELETE / It should delete the community", async () => {
    try {
      const resDeletedCommunity = await request(app)
        .delete(`${urlBase}/delete/${communityId}`)
        .set("Authorization", `Bearer ${token}`)

      expect(resDeletedCommunity.status).to.equal(200)

      const deletedCommunity = await Community.findById(communityId)
      const owner = await User.findById(user.id)

      expect(deletedCommunity).to.equal(null)
      expect(owner.Community).to.equal(undefined)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })

  // DELETE DUMMY USER
  after(async () => {
    try {
      await request(app)
        .delete(`/api/v1/user/delete/${user.id}`)
        .set("Authorization", `Bearer ${token}`)
    } catch (e) {
      if (e) {
        console.log(e)
      }
    }
  })
})
